package course.labs.activitylab;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class ActivityOne extends Activity {

		// string for logcat documentation
		private final static String TAG = "Lab-ActivityOne";

		// lifecycle counts
		//TODO: Create 7 counter variables, each corresponding to a different one of the lifecycle callback methods.
		//TODO:  increment the variables' values when their corresponding lifecycle methods get called.

		int onCreateCount = 0;
		int onStartCount = 0;
		int onResumeCount = 0;
		int onPauseCount = 0;
		int onStopCount = 0;
		int onDestroyCount = 0;
		int onRestartCount = 0;
		TextView onCreateCountView, onStartCountView, onResumeCountView, onPauseCountView, onStopCountView, onDestroyCountView, onRestartCountView;

		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_one);

			//Log cat print out
			Log.i(TAG, "onCreate called");

			onCreateCountView = (TextView) findViewById(R.id.createCount);
			onStartCountView = (TextView) findViewById(R.id.startCount);
			onResumeCountView = (TextView) findViewById(R.id.resumeCount);
			onPauseCountView = (TextView) findViewById(R.id.pauseCount);
			onStopCountView = (TextView) findViewById(R.id.stopCount);
			onDestroyCountView = (TextView) findViewById(R.id.destroyCount);
			onRestartCountView = (TextView) findViewById(R.id.restartCount);
			// updating the appropriate count variable & update the view
			onCreateCount += 1;
			onCreateCountView.setText(String.valueOf(onCreateCount));
		}

		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.activity_one, menu);
			return true;
		}
		
		// lifecycle callback overrides
		
		@Override
		public void onStart(){
			super.onStart();

			//Log cat print out
			Log.i(TAG, "onStart called");
			// updating the appropriate count variable & update the view
			onStartCount += 1;
			onStartCountView.setText(String.valueOf(onStartCount));
		}

	@Override
	public void onResume(){
		super.onResume();

		//Log cat print out
		Log.i(TAG, "onResume called");
		// updating the appropriate count variable & update the view
		onResumeCount += 1;
		onResumeCountView.setText(String.valueOf(onResumeCount));
	}

	@Override
	public void onPause(){
		super.onPause();

		//Log cat print out
		Log.i(TAG, "onPause called");
		// updating the appropriate count variable & update the view
		onPauseCount += 1;
		onPauseCountView.setText(String.valueOf(onPauseCount));
	}

	@Override
	public void onStop(){
		super.onStop();

		//Log cat print out
		Log.i(TAG, "onStop called");
		// updating the appropriate count variable & update the view
		onStopCount += 1;
		onStopCountView.setText(String.valueOf(onStopCount));
	}

	@Override
	public void onDestroy(){
		super.onDestroy();

		//Log cat print out
		Log.i(TAG, "onDestroy called");
		// updating the appropriate count variable & update the view
		onDestroyCount += 1;
		onDestroyCountView.setText(String.valueOf(onDestroyCount));
	}

	@Override
	public void onRestart(){
		super.onRestart();

		//Log cat print out
		Log.i(TAG, "onRestart called");
		// updating the appropriate count variable & update the view
		onRestartCount += 1;
		onRestartCountView.setText(String.valueOf(onRestartCount));
	}
	    // Note:  if you want to use a resource as a string you must do the following
	    //  getResources().getString(R.string.stringname)   returns a String.

		@Override
		public void onSaveInstanceState(Bundle savedInstanceState){
			//TODO:  save state information with a collection of key-value pairs & save all  count variables
		}


		public void launchActivityTwo(View view) {
			startActivity(new Intent(this, ActivityTwo.class));
		}
		

}
